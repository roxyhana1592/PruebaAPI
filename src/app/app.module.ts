import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import {TestService} from './test/test.service';
import { TestComponent } from './test/test.component';
import {Routes, RouterModule} from "@angular/router";

const appRoutes: Routes = [
  { path: 'test', component: TestComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    TestComponent
  ],
  imports: [
    HttpModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes, { useHash: true }),
    RouterModule.forChild(appRoutes)
  ],
  providers: [TestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
