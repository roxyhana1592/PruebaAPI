import { Component, OnInit } from '@angular/core';
import {TestService} from "./test.service";

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {

  testString : string = "";
  testString2 : string[] = [];

  constructor(public testService: TestService) { }

  ngOnInit() {

  }

  testing(): void{
    this.testService.getTest().then(t=>this.testString = t);
    this.testService.getTest2().then(t=>this.testString2 = t);
  }



}
