import { Injectable } from '@angular/core';
import {Http, RequestOptionsArgs} from '@angular/http';
import {ActivatedRoute} from '@angular/router';
import {Headers} from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class TestService {

  private TEST_API = 'api';
  private options: RequestOptionsArgs;

  constructor(private http: Http, public route: ActivatedRoute) {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    this.options = { headers : headers};
  }

  getTest() : Promise<string> {
    let url = `${this.TEST_API}/values/5`;
    return this.http.get(url, this.options)
      .toPromise().then(response => response.json())
      .catch(e => console.log(e))
  }
  getTest2() : Promise<string[]> {

    let url = `${this.TEST_API}/values`;
    return this.http.get(url, this.options)
      .toPromise().then(response => response.json())
      .catch(e => console.log(e))
  }


}
